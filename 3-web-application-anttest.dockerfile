# Step 1: Base image with Python runtime
FROM python:3.11-alpine

# Step 2: Set the working directory in the container
WORKDIR /app

# Step 3: Copy the Python files into the container
COPY Python-File/ /app

# Step 4: Update pip and install any dependencies required for the Python applications
RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir mysql-connector-python==8.0.27

# Step 5: Set environment variables for MySQL connection details
ENV DB_HOST=localhost \
    DB_NAME=anttest \
    DB_USER=root \
    DB_PASSWORD=!Kc971121

# Step 6: Define the command to run when the container starts
CMD ["python", "Hello-world.py"]
