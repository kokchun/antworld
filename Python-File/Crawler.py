import requests

def crawl_webpage(url):
    try:
        # Send a GET request to the URL
        response = requests.get(url)
        
        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            # Print the HTML content of the webpage
            print(response.text)
        else:
            print(f"Failed to fetch URL: {url}. Status code: {response.status_code}")
    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    # URL of the webpage to crawl
    url = "https://www.aftershockpc.com.my/"
    
    # Call the crawl_webpage function with the URL
    crawl_webpage(url)
