'''Due to i'm using windows 10 desktop, so i through powershell pip install psutil library '''
import psutil

'''check for cpu percent , interval which is check for how many seconds later (here check with 1 seconds usage of CPU))''' 
def get_cpu_usage():
    return psutil.cpu_percent(interval=1)

'''check for ram usage %'''
def get_ram_usage():
    return psutil.virtual_memory().percent

'''def the name main as function and declare cpu + ram usage with get cpu+ram variable, cout the result'''
def main():
    cpu_usage = get_cpu_usage()
    ram_usage = get_ram_usage()
    print(f"CPU Usage: {cpu_usage}%")
    print(f"RAM Usage: {ram_usage}%")

'''run direct through main function'''
if __name__ == "__main__":
    main()
