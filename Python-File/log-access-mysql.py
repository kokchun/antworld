import mysql.connector
from datetime import datetime

# MySQL connection details
DB_HOST = 'localhost'
DB_NAME = 'anttest'
DB_USER = 'root'
DB_PASSWORD = '!Kc971121'

# Connect to MySQL
def connect_to_mysql():
    try:
        conn = mysql.connector.connect(host=DB_HOST, database=DB_NAME, user=DB_USER, password=DB_PASSWORD)
        print("Successfully connected to MySQL database")
        return conn
    except Exception as e:
        print(f"Error connecting to MySQL: {e}")
        return None

# Read access log data from MySQL
def read_access_logs():
    conn = connect_to_mysql()
    if conn:
        try:
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM access_logs")
            rows = cursor.fetchall()
            for row in rows:
                # Convert DATETIME to a formatted string (e.g., 'YYYY-MM-DD HH:MM:SS')
                formatted_datetime = row[1].strftime('%Y-%m-%d %H:%M:%S')
                print((row[0], formatted_datetime, row[2]))  # Print each row with formatted DATETIME
        except Exception as e:
            print(f"Error reading access logs from MySQL: {e}")
        finally:
            cursor.close()
            conn.close()

# Main function
def main():
    read_access_logs()

if __name__ == '__main__':
    main()
